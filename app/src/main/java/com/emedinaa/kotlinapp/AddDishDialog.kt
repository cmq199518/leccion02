package com.emedinaa.kotlinapp

import android.app.Dialog
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import com.emedinaa.kotlinapp.databinding.DialogAddDishBinding
import com.emedinaa.kotlinapp.databinding.FragmentChefBinding

class AddDishDialog : DialogFragment() {

    private var _binding: DialogAddDishBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = DialogAddDishBinding.inflate(inflater, container, false)
        binding.cardView.setBackgroundDrawable(null);

        return binding.root
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return activity?.let {
            val builder = AlertDialog.Builder(it)
            val inflater = requireActivity().layoutInflater;
            builder.setView(inflater.inflate(R.layout.dialog_add_dish, null))
            builder.create()
        } ?: throw IllegalStateException("Activity cannot be null")
    }

}